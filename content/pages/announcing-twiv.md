---
title: "Announcing TWIVelorcraft"
date: 2021-07-01T11:03:08Z
draft: false
---

# Announcing This Week In Velorcraft

Just like in Veloren, every week I will post changes to the velocraft site! You can find them in timeline [here]({{< ref "/timeline" >}} "Timeline")

# A quick reminder

Velorcraft is still an early project! The server will probably be reset multiple times before anything really kicks off. It will have problems. Please do us a favour by reporting these on the gitlab issue tracker [here](https://gitlab.com/kuroyuki-simp/velorcraft-site/-/issues)