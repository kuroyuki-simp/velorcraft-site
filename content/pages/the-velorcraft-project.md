---
title: "The Velorcraft Project"
date: 2021-06-29T19:47:16+01:00
description: "A brief introduction to the velorcraft project"
summary: "A brief introduction to the velorcraft project"
draft: false
---

# The velorcraft project

This page uses the "5 Ws". Throwback to literacy class 😅

## Who?

The velorcraft project is made by me and my friends

## What?

Velorcraft attempts to create a minecraft server like the open source cube-world inspired block RPG game, Veloren.

## When?

You can view the project timeline [here]({{< ref "/timeline" >}} "Timeline")

## Where?

Velorcraft is hosted at me.yoshichill.uk (up when I pay the bills)

## Why?

Velorcraft's idea was started on the day this article was written. I had always wanted to create a fun and unique minecraft experience, and the idea came to me when I thought that I wanted airships in minecraft. Then I remembered what else has airships: Veloren! Thus, velorcraft was born. Will there be airships in velorcraft? Unlikely, because it can cause lag and de-sync will result in players falling off of an airship without a parachute. However, if I find a way (or another plugin I can borrow code from / use it's API), then I will waste at least 12 hours creating airships.
