---
title: "29th June - The idea arose"
date: 2021-06-29T19:47:16+01:00
description: "The birth of velorcraft"
summary: "When velorcraft was born"
draft: false
---

# 29th June
Velorcraft's idea was created.
Excerpt from velorcraft's introduction post
> I had always wanted to create a fun and unique minecraft experience, and the idea came to me when I thought that I wanted airships in minecraft. Then I remembered what else has airships: Veloren! Thus, velorcraft was born.