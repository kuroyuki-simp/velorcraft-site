---
title: "This Week In Velorcraft 1"
date: 2021-07-01T11:25:27Z
draft: false
---

# 1. Table of contents
- [1. Table of contents](#1-table-of-contents)
- [2. NPCs](#2-npcs)
- [3. World Generation](#3-world-generation)
- [4. A well shaped stone](#4-a-well-shaped-stone)
- [5. A quick reminder](#5-a-quick-reminder)

# 2. NPCs

This week we add NPCs! In the starter town, a guard will patrol and kill anyone who attacks anything in the area. So make sure you watch your fire!
Also, a trader NPC will visit players and offer items! (soon)

# 3. World Generation

The world is now generated using the spigot plugin Terra and its default config pack. This will probably be tweaked later to be more like Veloren, but it is unlikely we will get near-identical generation with things like paths.

# 4. A well shaped stone

When you spawn, you get some basic stone tools. What a time saver! I might expand this kit later.

# 5. A quick reminder

Velorcraft is still an early project! The server will probably be reset multiple times before anything really kicks off. It will have problems. Please do us a favour by reporting these on the gitlab issue tracker [here](https://gitlab.com/kuroyuki-simp/velorcraft-site/-/issues)